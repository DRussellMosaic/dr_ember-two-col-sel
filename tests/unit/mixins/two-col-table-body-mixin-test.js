import EmberObject from '@ember/object';
import TwoColTableBodyMixinMixin from 'ember-two-col-select/mixins/two-col-table-body-mixin';
import { module, test } from 'qunit';

module('Unit | Mixin | two-col-table-body-mixin', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let TwoColTableBodyMixinObject = EmberObject.extend(TwoColTableBodyMixinMixin);
    let subject = TwoColTableBodyMixinObject.create();
    assert.ok(subject);
  });
});
