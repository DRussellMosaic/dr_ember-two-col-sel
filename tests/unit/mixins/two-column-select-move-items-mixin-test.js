import EmberObject from '@ember/object';
import TwoColumnSelectMoveItemsMixinMixin from 'ember-two-col-select/mixins/two-column-select-move-items-mixin';
import { module, test } from 'qunit';

module('Unit | Mixin | two-column-select-move-items-mixin', function() {
  // Replace this with your real tests.
  test('it works', function (assert) {
    let TwoColumnSelectMoveItemsMixinObject = EmberObject.extend(TwoColumnSelectMoveItemsMixinMixin);
    let subject = TwoColumnSelectMoveItemsMixinObject.create();
    assert.ok(subject);
  });
});
