import Route from '@ember/routing/route';
import RSVP from 'rsvp';
import { A } from '@ember/array';
import EmberObject from '@ember/object';
import faker from 'faker';
import $ from 'jquery';

export default Route.extend({

  model(){
    let leftFilters = this.buildFilter();
    var leftTableDataList = A ([]);
    var rightTableDataList = A ([]);
    var object = EmberObject.extend({});

    for(let i=0; i< 100; i++){

      let icons = [
        {
          icon: "fa-info-circle",
          title: "this is a circle",
          message: "this is the circle message",
          class: "blue"
        },
        {
          icon: "fa-info-circle",
          modal: true,
          modalId: "courseDescriptionModal_"+i,
          title: "modal test",
          subtitle: "modal test",
          rows: [
            {
              title: 'row 1',
              value: 'blah',
              type: 'text'
            },
            {
              title: 'row 2',
              value: ['fa-info-circle', 'fa-arrows'],
              type: 'icons'
            },
            {
              title: 'row 3',
              value: ['item 1', 'item 2', 'item 3'],
              type: 'ul'
            },
            {
              title: 'row 3',
              value: ['item 1', 'item 2', 'item 3'],
              type: 'ol'
            }
          ],
          class: "red"
        }
      ];

      if(i % 2 === 0){
        icons.push(    {
              icon: "fa-arrows",
              title: "this is a arrows",
              message: "this is the arrows message"
            }) ;
      }
      var rowData = object.create();
      rowData.set("id",i);
      rowData.set("id",i);
      rowData.set("lic",30);
      rowData.set("name",faker.name.firstName());
      rowData.set("place","home");
      rowData.set("disabled",false);
      //rowData.set("iconList", icons);
      rowData.set("leftIcon", "fa-user");
      rowData.set('iconList', icons);

      if(i % 4 === 0){
        rowData.set("disabled",true);
      }



      if(i <= 49){
        rightTableDataList.push(rowData);
      }
      else{
        leftTableDataList.push(rowData);
      }

  }

    return RSVP.hash({
      leftTableDataList:leftTableDataList,
      rightTableDataList:rightTableDataList,
      leftFilters: leftFilters
    });

  },

  buildFilter: function(){
  var userCategoryFilter = {
      type: 'multi-select',
      label: 'User Type',
      name: 'place',
      filterSelectContent: [
        {'value' : 'work', 'name': 'Work'},
        {'value' : 'home', 'name': 'Home'},

      ],
      filterTarget: 'place',
      filterTargetVar: 'place',
      filterTargetDepth: 0,
      selectedItems: [],
    };


    var nameFilter = {
      type: 'text',
      label: 'Name',
      name: 'name',
      filterTarget: ['name'],
      searchValue: null
    };

    var disabledFilter =  {
            type: 'checkbox',
            name: 'disabled',
            label: 'Disabled',
            filterTargetVar: 'disabled',
            filterActionSwitch: true,//true means compute filter on checked, otherwise show all. false means compute filter on unchecked, otherwise show all.
            filterCheckbox: false,
            filterTarget: true //value you are checking for to show if filterActionSwitch == filterCheckbox
          };

    return [
      JSON.parse(JSON.stringify(userCategoryFilter)),
      JSON.parse(JSON.stringify(disabledFilter)),
      nameFilter
    ];
  }


});
