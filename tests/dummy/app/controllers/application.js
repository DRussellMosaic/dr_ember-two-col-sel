import Controller from '@ember/controller';
import TwoColMoveMixin from 'ember-two-col-select/mixins/two-column-select-move-items-mixin';
import { computed } from '@ember/object';
import MultisearchFilterMixin from 'ember-multisearch-sortable-tables/mixins/multisearch-filter-mixin';
import { A } from '@ember/array';

export default Controller.extend(TwoColMoveMixin,MultisearchFilterMixin,{
    leftColumnTitle: 'Available Courses',
    rightColumnTitle: 'Selected Courses',
    twoColLeftSideData: 'model.leftTableDataList',
    twoColRightSideData: 'model.rightTableDataList',
    twoColSelectLabels: null,
    leftFilters: [],
    rightFilters: [],
    sortKey: "name",
    filters: null,
    col: null,
    additionalCols: null, 
    header: null,
    headers: null,
    additionalCols: null, 

    //properties for testing our dynamic title property
    randomNum: 1,

    leftColumnTitleAttrs: computed('randomNum', function(){
      let icons = [
        {
          icon: "fa-info-circle",
          title: "this is a circle",
          message: "this is the circle message",
          class: "blue"
        },
        {
          icon: "fa-info-circle",
          modal: true,
          modalId: "courseDescriptionModal_"+this.get('randomNum'),
          title: "modal test",
          subtitle: "modal test",
          rows: [
            {
              title: 'row 1',
              value: 'blah',
              type: 'text'
            },
            {
              title: 'row 2',
              value: ['fa-info-circle', 'fa-arrows'],
              type: 'icons'
            },
            {
              title: 'row 3',
              value: ['item 1', 'item 2', 'item 3'],
              type: 'ul'
            },
            {
              title: 'row 3',
              value: ['item 1', 'item 2', 'item 3'],
              type: 'ol'
            }
          ],
          class: "red"
        }
      ];
      // this can be text, which support dynamic properties or an icon list.
      // @todo - need to get the icons displaying in same row.
      return [
        {text: 'Static title'},
        {text: this.get('randomNum')},
        {iconList: icons}
      ]
    }),


    init() {


      this._super(...arguments);

      let additionalCol = "lic";
      let additionalCols = [
        { name: "lic", iconColumnList: "fa-info-circle" },
        { name: "lic" },
        { iconColumnList: 'iconList' },
        { iconListProperty: 'iconList' }
      ];

      let headers = ["Header1","Header 2","Header 3", "Header 4"];

      var filters = [
          {
          searchTarget: 'place',
          content: [
            {'value' : 'work', 'name': 'Work'},
            {'value' : 'home', 'name': 'Home'},
          ],
          multiselectValue: A([]),
          searchPlaceHolder: 'place',
          type: 'multiselect',
          checked: true,
          optionValuePath: 'value',
          optionLabelPath: 'name',
        },

        {
          searchTarget: 'name',
          searchPlaceHolder: 'First Name',
          checked: false
        }
      ];

      this.set("col",additionalCol);
      this.set("additionalCols", additionalCols);
      this.set("headers", headers);
      this.set("additionalCols", additionalCols);
      this.set("headers", headers);
      this.set("filters",filters);

  },


  actions:{
    //this action is a control to prove our title properties that are text can be dynamic.
    incrementNum(){
      this.set('randomNum', this.get('randomNum') + 1)
    }, 
    moveEachItem(itemsToMove, source, dest){
      itemsToMove.forEach(function(item) {
        dest.addObject(item);
        source.removeObject(item);
        item.set('checked', false);
      }.bind(this));    }
  }

});
