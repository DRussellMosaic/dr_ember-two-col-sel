export default function strtrim(value) {
  value = value ? value.trim() : "";
  value = value.replace(/ +(?= )/g,'');

  return value;
}
