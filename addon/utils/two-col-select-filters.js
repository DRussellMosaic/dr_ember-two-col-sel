import { A } from '@ember/array';
import EmberObject from '@ember/object';
import TextFilter from '../utils/text-filter';
import CheckboxFilter from '../utils/checkbox-filter';
import DateRangeFilter from '../utils/date-range-filter';
import MultiSelectFilter from '../utils/multi-select-filter';

export default EmberObject.extend({
  init: function(){
    this._super();
    this.set('filters', A());
  },
  getFilters: function(){
    return this.get('filters');
  },
  getCheckbox: function(){
    return this.get('filters').findBy("type", "checkbox");
  },
  loadFilters: function(array){
    array.forEach(function(filter){
      switch (filter.type) {
        case "multi-select":
            var miltSelectFilter = MultiSelectFilter.create();
            miltSelectFilter.loadFilter(filter);
            this.getFilters().addObject(miltSelectFilter);
          break;
        case "date-range":
            var dateRangeFilter = DateRangeFilter.create();
            dateRangeFilter.loadFilter(filter);
            this.getFilters().addObject(dateRangeFilter);
          break;
        case "checkbox":
            var checkboxFilter = CheckboxFilter.create();
            checkboxFilter.loadFilter(filter);
            this.getFilters().addObject(checkboxFilter);
          break;
        case "text":
            var textFilter = TextFilter.create();
            textFilter.loadFilter(filter);
            this.getFilters().addObject(textFilter);
          break;
        default:
      }
    }.bind(this));
  }
});
