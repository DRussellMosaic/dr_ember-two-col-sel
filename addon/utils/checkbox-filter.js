import FilterObject from '../utils/filter-object';

export default FilterObject.extend({
  init: function(){
    this._super();
    this.setProperties({
      type: 'checkbox',
      label: '',
      name: '',
      filterActionSwitch: false,
      filterCheckbox: false,
      filterTarget: false,
      filterTargetVar: ''
    });
  },
  check: function(){
    this.set('filterCheckbox', ! this.get('filterCheckbox'));
  }
});
