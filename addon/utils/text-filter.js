import { A } from '@ember/array';
import FilterObject from '../utils/filter-object';

export default FilterObject.extend({
  init: function(){
    this._super();
    this.setProperties({
      type: 'text',
      label: '',
      name: '',
      searchValue: null,
      filterTarget: A()
    });
  }
});
