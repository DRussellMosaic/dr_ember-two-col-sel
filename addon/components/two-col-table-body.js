import Component from '@ember/component';
import { computed } from '@ember/object';
import TwoColTableBodyMixin from  '../mixins/two-col-table-body-mixin';

export default Component.extend(TwoColTableBodyMixin, {
  rowCap: 50,
  triggerOffset: 15,
  triggerLoadRow: computed('rowCap', 'triggerOffset', function(){
    return this.get("rowCap") - this.get("triggerOffset");
  }),
});
