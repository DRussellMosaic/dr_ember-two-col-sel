import Component from '@ember/component';

export default Component.extend({
  icons:  { grid: {
      expand: 'chevron-right',
      collapse: 'chevron-down',
      editHover: 'pencil',
      actionHover: 'chevron-right',
      cartExpand: 'caret-right',
      cartCollapse: 'caret-down',
      twoColSelectExpand: 'caret-right',
      twoColSelectCollapse: 'caret-down'
    }},


  init() {
    this._super(...arguments);
  },
  actions:{
    toggleFilters() {
      this.toggleFilters();
    }
  }
});
