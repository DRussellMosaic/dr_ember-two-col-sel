import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  checkBoxVal: false,

  isEmpty: computed('tableData.[]', 'tableData.@each.disabled', function() {
    let allDisabled = true;
    this.get('tableData').forEach(item => {
      if(!item.get('disabled')){
        allDisabled = false;
      }
    });
    return !this.get('tableData.length') || allDisabled;
  }),

  actions: {
    checkAll: function(tableData) {
      const checked = !this.get('checkBoxVal');
      this.set('checkBoxVal', checked);

      tableData.forEach(item => {
        if (!item.get('disabled') && !item.get('disabledCheckAll')) {
          item.set('checked', checked);
        }
      });
    }
  }
});
