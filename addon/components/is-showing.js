import Component from '@ember/component';
import layout from '../templates/components/is-showing';
import InViewportMixin from 'ember-in-viewport';

export default Component.extend(InViewportMixin, {
  tagName: '',

  didEnterViewport() {
    this.get('on-enter')();
  },
});
