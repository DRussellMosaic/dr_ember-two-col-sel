import Component from '@ember/component';
import TwoColSelectData from '../utils/two-col-select-data';
import TwoColSelectFilters from '../utils/two-col-select-filters';

export default Component.extend({
  tagName: 'tr',

  actions: {

    cancelDatePicker: function(){
      this.setDateIfBlank();
    },

    checkAll: function(checkBox, tableData) {
      this.set(checkBox, !this.get(checkBox));
      tableData.forEach(function(item) {
        if (!item.get('disabled') && !item.get("disabledCheckAll")) {
          item.set('checked', this.get(checkBox));
        }
      }.bind(this));
    },

    checkFilter: function (filter) {
      filter.check()
    },

    clearDateFilters: function(filter) {
      filter.set('dateTo', null);
      filter.set('dateFrom', null);
    },

    toggleFilters: function() {
      this.set('showFilters', !this.get('showFilters'));
    },

    updateSelectFilter: function(filter){
      this.updateSelectFilter(filter);
    },

  }
});
