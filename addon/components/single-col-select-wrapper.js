import Component from '@ember/component';

export default Component.extend({
  tagName: 'div',
  mouseUp: function() {
    if(this.get('mouseDragData.dragging')){
      //move them over to the other side
      this.send('moveItems');
      this.clearSelection();
    }
    return true;
  },
  mouseLeave: function(){
    this.clearSelection();
  },
  clearSelection(){
    $('.selected').removeClass('selected');
    this.set('mouseDragData', {
      'selectedRows': [],
      'selectedIds': [],
      'dragging': false
    });
  },
  actions:{
    moveItems: function(){
      this.sendAction('moveItemsUp', this.get('mouseDragData.selectedIds'));
    }
  }
});
