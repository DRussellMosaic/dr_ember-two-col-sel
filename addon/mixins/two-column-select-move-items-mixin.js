import Mixin from '@ember/object/mixin';
import { inject as service } from '@ember/service';

export default Mixin.create({
  store: service('store'),
  twoColLeftSideData: '',
  twoColRightSideData: '',
  moodleUsersMove: false,//set true if the
  actions: {
    moveItems(itemsToMove, sourceSide){
      if(this.get('moodleUsersMove')){
        this.send('moveMoodleUsers', itemsToMove, sourceSide);
      }else{
        var source, dest;
        if(sourceSide === "left"){
          source = this.get(this.get('twoColLeftSideData'));
          dest = this.get(this.get('twoColRightSideData'));
        }else{
          source = this.get(this.get('twoColRightSideData'));
          dest = this.get(this.get('twoColLeftSideData'));
        }

        itemsToMove.forEach(function(item){
          dest.addObject(item);
          source.removeObject(item);
        }.bind(this));
      }
    },
    moveMoodleUsers(itemsToMove, sourceSide){
      var source, dest;
      if(sourceSide === "left"){
        source = this.get(this.get('twoColLeftSideData'));
        dest = this.get(this.get('twoColRightSideData'));

        itemsToMove.forEach(function(item){
          //need to add the moodleUser with the same ID. An LMS-User object can not live in the filterGroup.relationships.moodleUsers
          //note reload:false and backgroundReload:false are here to prevent any background reloads of a moodleUser, which
          //causes the two column select left/right data to be reset
          this.get('store').findRecord('moodleUser', item.get('id'), {
            reload: false, backgroundReload: false
          }).then(function(moodleUser){
              if(typeof item.icon !== undefined){
                moodleUser.set('icon', item.icon);
              }
              dest.addObject(moodleUser);
          }).catch(function(){
            //if the moodleUser does not exist in our DB, than create it.
            var moodleUser = this.get('store').createRecord('moodle-user', {
              id: item.get('id')
            });
            moodleUser.save().then(function(moodleUser){
              if(typeof item.icon !== undefined){
                moodleUser.set('icon', item.icon);
              }
              dest.addObject(moodleUser);
            });
          }.bind(this));

          source.removeObject(item);
        }.bind(this));

      }else{
        source = this.get(this.get('twoColRightSideData'));
        dest = this.get(this.get('twoColLeftSideData'));

        itemsToMove.forEach(function(item){
          //depending on if the object being moved, do differnt logic
          var objectToRemove;
          var moodleUser;

          switch (item.constructor.modelName) {
            case "lms-user":
              moodleUser = this.get('store').peekRecord('lms-user', item.get('id'));
              objectToRemove = this.get('store').peekRecord('moodle-user', item.get('id'));
              break;

            case "pending-enrollment":
              moodleUser = this.get('store').peekRecord('lms-user', item.get('lmsUserId'));
              objectToRemove = item;
              break;

            default:
              moodleUser = this.get('store').peekRecord('lms-user', item.get('id'));
              objectToRemove = item;
          }

          dest.addObject(moodleUser);
          //some two column selects store moodleUsers on right side, some store lms-user.
          source.removeObjects([moodleUser, objectToRemove]);
        }.bind(this));
      }
    },
  }
});
